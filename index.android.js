/*
 * Module: GoodFarmers
 * Author: Victor Huberta & Daniel Jonathan
 *
 * # Description
 * A mobile application to educate Malaysian
 * dairy farmers on dairy best practices.
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Image,
  Text,
  View,
  StatusBar
} from 'react-native';

import SplashScreen from './App/Components/Splash/SplashScreen';
import MainMenu from './App/Components/Menu/MainMenu';
import MenuContent from './App/Components/Menu/MenuContent';
import MoreInfo from './App/Components/Menu/MoreInfo';

var GLOBAL = require('./App/Globals');

class GoodFarmers extends Component {
  /*
   * Navigate all pages by page id and
   * pass content to each page.
   */

  constructor(props) {
    super(props);

    StatusBar.setBackgroundColor('#1a8e77');

    // To be used by navigator
    this.state = {
      pages: [{
          pageId: GLOBAL.SPLASH_SCREEN, // Initial page
          content: {}, // Content for each page
          image: {}
      }]
    };
  }

  render() {
    var page = this.state.pages[this.state.pages.length - 1];

    switch (page.pageId) {
      case GLOBAL.SPLASH_SCREEN:
        return this._showSplashScreen(page);
      case GLOBAL.MAIN_MENU:
        return this._showMainMenu(page);
      case GLOBAL.MENU_CONTENT:
        return this._showMenuContent(page);
      case GLOBAL.MORE_INFO:
        return this._showMoreInfo(page);
      default:
        return null;
    }
  }

  _showSplashScreen(page) {
    // Change to main menu page after 2 seconds.
    setTimeout(() => {
      this.setState({
        pages: this.state.pages.concat([{
          pageId: GLOBAL.MAIN_MENU
        }])
      });
    }, 2000);

    return (
      <SplashScreen />
    );
  }

  _showMainMenu(page) {
    return (
      <MainMenu navigator={this} />
    );
  }

  _showMenuContent(page) {
    return (
      <MenuContent navigator={this} content={page.content} image={page.image} />
    );
  }

  _showMoreInfo(page) {
    return (
      <MoreInfo navigator={this} info={page.info} />
    );
  }
}

AppRegistry.registerComponent('GoodFarmers', () => GoodFarmers);
