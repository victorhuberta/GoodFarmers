import React, { Component } from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  Image,
  TouchableOpacity
} from 'react-native';

var GLOBAL = require('../../Globals');
var menuOptions = require('../../Content/MenuOptions');

import MenuOption from './MenuOption';

export default class MainMenu extends Component {
  /*
   * Show all menu options, and ask navigator to
   * load new page when user selects an option.
   */

  _getAllOptions() {
    var options = [];

    for (var option of Object.keys(menuOptions)) {
      options.push(
        <MenuOption
          key={menuOptions[option].title}
          navigator={this.props.navigator}
          option={menuOptions[option]} />
      );
    }

    return options;
  }

  render() {
    return(
      <ScrollView contentContainerStyle={styles.scrollView} >
        {this._getAllOptions()}
      </ScrollView>

    );
  }

}

var styles = StyleSheet.create({
  scrollView: {}
})
