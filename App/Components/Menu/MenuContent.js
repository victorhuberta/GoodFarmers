import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Image,
  ScrollView,
  Dimensions
} from 'react-native';

var GLOBAL = require('../../Globals');

export default class MenuContent extends Component {
  /*
   * View the title, description, and all needed
   * steps for a particular milking hygene lesson.
   */

  _getAllSteps() {
    var steps = [];

    for (var i in this.props.content.steps) {
      var step = this.props.content.steps[i];

      steps.push(
        <View key={i}>
          <View style={styles.headerBackground} >
            <Text style={styles.stepTitle}>{'STEP ' + (parseInt(i) + 1)}</Text>
          </View>

          <TouchableOpacity onPress={this._navigateToMoreInfoPage(step)}>
            <View style={styles.imageAndContent}>
                <Image source={step.image} style={styles.stepImage} />
                <Text style={styles.stepContent}>{step.text}</Text>
                {this._getMoreInfoIndicator(step)}
            </View>
          </TouchableOpacity>
        </View>
      );
    }

    return steps;
  }

  /*
   * Get the image of more info indicator.
   */
  _getMoreInfoIndicator(step) {
    return (Object.keys(step.more_info).length > 0) ?
      <Image source={require('./info_icon.png')}
        style={styles.infoIcon} />
      : null
  }

  /*
   * Navigate to more info page if there is more information.
   */
   _navigateToMoreInfoPage(step) {
    return (() => {
      if (Object.keys(step.more_info).length > 0) {
        this.props.navigator.setState({
          pages: this.props.navigator.state.pages.concat([{
            pageId: GLOBAL.MORE_INFO,
            info: step.more_info
          }])
        });
      }
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.bar}>
          <TouchableOpacity onPress={() => {
            this.props.navigator.setState({
              pages: this.props.navigator.state.pages.slice(0, -1)
            });
          }}>
              <Image style={styles.backBtnImg} source={require('./back_btn.png')}>
              </Image>
            </TouchableOpacity>
          </View>
        <ScrollView style={styles.scrollContainer}>
          <Image source={this.props.image} style={styles.headerImage}>
            <Text style={styles.title}>{this.props.content.title}</Text>
          </Image>
          <View style={styles.descBackground}>
            <Text style={styles.contentDesc}>
              {this.props.content.description}
            </Text>
          </View>
          <View style={styles.headerBackground}>
            <Text style={styles.stepHeader}>BEST PRACTICES</Text>
          </View>
          {this._getAllSteps()}
        </ScrollView>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bar: {
    backgroundColor: '#1a8e77',
    padding: 10,
  },
  scrollContainer: {
    flexDirection: 'column',
  },
  headerBackground: {
    backgroundColor: '#1a8e77',
    padding: 20,
  },
  contentDesc: {
    fontSize: 16,
  },
  headerImage: {
    backgroundColor: '#000000',
    flexDirection: 'column',
    justifyContent: 'center',
    opacity: 0.6,
    width: Dimensions.get('window').width,
    height: 200,
  },
  descBackground: {
    backgroundColor: '#ffffff',
    padding: 20,
    alignItems: 'center',
  },
  stepHeader: {
    textAlign: 'center',
    color: '#99ddcf',
    fontSize: 26,
  },
  stepTitle: {
    color: '#ffffff',
    textAlign: 'center',
    fontSize: 23,
  },
  stepImage: {
    transform: [{
      scale: 0.75,
    }],
    flex: 0.9,
  },
  stepContent: {
    textAlign: 'center',
    flex: 1.25,
    fontSize: 16,
    marginRight: 5,
  },
  infoIcon: {
    flex: 0.2,
    width: 20,
    height: 20
  },
  title: {
    color: '#ffffff',
    fontSize: 30,
    fontWeight: '100',
    textAlign: 'center',
    marginRight: 5
  },
  imageAndContent: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  backBtnImg: {
    width: 40,
    height: 40
  }
});
