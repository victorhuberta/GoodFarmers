import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Image,
  ScrollView,
  Dimensions
} from 'react-native';

export default class MoreInfo extends Component {

  constructor(props) {
    super(props);
    this.state = {
      imgWidth: Dimensions.get('window').width
    };
  }

  onImageLoaded(obj) {
    return function onLayoutCallback(data) {
      obj.setState({
        imgWidth: data.nativeEvent.layout.width
      });
    };
  }

  _getInfoImageStyle() {
    return {
      transform: [{
        scale: Dimensions.get('window').width / this.state.imgWidth
      }],
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.bar}>
          <TouchableOpacity onPress={() => {
            this.props.navigator.setState({
              pages: this.props.navigator.state.pages.slice(0, -1)
            });
          }}>
              <Image style={styles.backBtnImg} source={require('./back_btn.png')}>
              </Image>
          </TouchableOpacity>
        </View>
        <ScrollView contentContainerStyle = {styles.scrollContainer}>
          <Text style={styles.infoText}>{this.props.info.text}</Text>
          <Image source={this.props.info.image}
            onLayout={this.onImageLoaded(this)}
            style={this._getInfoImageStyle()}>
          </Image>
        </ScrollView>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bar: {
    backgroundColor: '#1a8e77',
    padding: 10,
  },
  scrollContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  backBtnImg: {
    width: 40,
    height: 40
  },
  infoText: {
    fontSize: 16,
    marginLeft: 5,
  }
});
