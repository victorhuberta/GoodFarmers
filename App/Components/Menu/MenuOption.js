import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  TouchableOpacity,
  Dimensions
} from 'react-native';

var GLOBAL = require('../../Globals');

export default class MenuOption extends React.Component {
  /*
   * View a menu option with title and image to
   * be used by MainMenu class.
   */

  render() {
    return (
      // Notify navigator to load new page and pass content
      <TouchableOpacity onPress={() => {
        this.props.navigator.setState({
          pages: this.props.navigator.state.pages.concat({
            pageId: GLOBAL.MENU_CONTENT,
            content: this.props.option.content,
            image: this.props.option.image
          })
        });
      }}>
        <Image resizeMode='cover' style={styles.img} source={this.props.option.image}>
          <View style={styles.titleBar}>
            <Text style={styles.optionTitle}>{this.props.option.title}</Text>
          </View>
        </Image>
      </TouchableOpacity>
    );
  }
}

var styles = StyleSheet.create({
  img: {
    flexDirection: 'column',
    width: Dimensions.get('window').width,
    justifyContent: 'flex-end'
  },
  titleBar: {
    backgroundColor: 'rgba(26, 142, 119, 0.6)',
    // backgroundColor: 'rgba(153, 221, 207, 0.6)',
    padding: 10
  },
  optionTitle: {
    color: '#ffffff',
    fontSize: 26,
    fontWeight: '100'
  },
});
