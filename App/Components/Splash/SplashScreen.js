import React, { Component } from 'react';
import {
  StyleSheet,
  Image,
  Text,
  View
} from 'react-native';

export default class SplashScreen extends Component {
  /*
   * Show a cool logo on app launch.
   */

  render() {
    return (
    <View style={styles.container}>
      <Image
        style={styles.image}
        source={require('./gf_logo.png')}
      />
    </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    backgroundColor: '#1a8e77',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  },
  image: {
    width: 175,
    height: 80
  }
});
