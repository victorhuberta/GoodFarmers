module.exports = {
  title: 'Pemerahan Susu Yang Bersih',
  description: `
  PERALATAN YANG DIPERLUKAN
    # tuala bersih atau tuala keras
    # air bersih
    # baldi Keluli (stainless steel)
    # Mesin pemerah susu
    # Tali (untuk mengikat ekor haiwan)
  `,
  steps: [
    {
      image: require('./pekerja_2.png'),
      text: 'Basuh tangan dengan kaedah yang betul.',
      more_info: {
        image: require('./basuh_tangan.png'),
        text: 'Basuh tangan dengan kaedah yang betul.'
      }
    },

    {
      image: require('./pemerahan_2.png'),
      text: 'Basuh ambing susu dengan air bersih.',
      more_info: {}
    },

    {
      image: require('./pemerahan_3.png'),
      text: 'Keringkan ambing dengan tuala.',
      more_info: {}
    },

    {
      image: require('./pemerahan_4.png'),
      text: 'Jalankan ujian California Mastitis Test (CMT).',
      more_info: {}
    },

    {
      image: require('./pemerahan_5.png'),
      text: 'Mulakan pemerahan.',
      more_info: {}
    },
  ]
};
