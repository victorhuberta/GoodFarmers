module.exports = {
  Pekerja: {
    title: 'Pekerja',
    content: require('./Pekerja'),
    image: require('./pekerja_option.jpg')
  },
  PemerahanSusu: {
    title: 'Pemerahan Susu Yang Bersih',
    content: require('./PemerahanSusu'),
    image: require('./pemerahan_susu_bersih_option.jpg')
  },
  PengendalianSusu: {
    title: 'Pengendalian Susu Yang Diperah',
    content: require('./PengendalianSusu'),
    image: require('./pengendalian_susu_option.jpg')
  },
  InfrastukturPemprosesanSusu: {
    title: 'Infrastruktur Pemprosesan Susu',
    content: require('./InfrastukturPemprosesanSusu'),
    image: require('./infrastruktur_pemprosesan_susu_option.jpg')
  },
  KebersihanPengendalianSusu: {
    title: 'Kebersihan Pengendalian Pempasteuran Susu',
    content: require('./KebersihanPengendalianSusu'),
    image: require('./kebersihan_pengendalian_susu_option.jpg')
  },
  PenerimaanSusu: {
    title: 'Penerimaan Susu',
    content: require('./PenerimaanSusu'),
    image: require('./penerimaan_susu_option.jpg')
  },
  PembotolanSusu: {
    title: 'Pembotolan & Pelabelan Susu',
    content: require('./PembotolanSusu'),
    image: require('./pembotolan_susu_option.jpg')
  },
  PenyimpananSusu: {
    title: 'Penyimpanan & Pengedaran Susu',
    content: require('./PenyimpananSusu'),
    image: require('./penyimpanan_susu_option.jpg')
  }
};
