module.exports = {
  title: 'Pembotolan & Pelabelan Susu',
  description: ``,
  steps: [
    {
      image: require('./pembotol_1.png'),
      text: 'Susu hendaklah dibotolkan dengan segera selepas susu mencapai suhu 35 °C.',
      more_info: {}
    },

    {
      image: require('./pembotol_2.png'),
      text: 'Susu boleh dibotolkan menggunakan botol kaca / plastic.',
      more_info: {}
    },

    {
      image: require('./pembotol_3.png'),
      text: 'Botol susu hendaklah dilabelkan dengan maklumat tertentu.',
      more_info: {
        image: {},
        text: `
a) Nama produk
b) Kandungan nutrisi
c) Tarikh luput
d) Suhu penyimpanan
e) Harga jualan`
      }
    }
  ]
};
