module.exports = {
  title: 'Penyimpanan & Pengedaran Susu',
  description: ``,
  steps: [
    {
      image: require('./pengendalian_9.png'),
      text: 'Susu hendaklah diisi dalam bekas yang bersih dan disimpan pada suhu 4°C sebelum diedarkan.',
      more_info: {}
    },

    {
      image: require('./penyimpanan_2.png'),
      text: 'Tarikh penyimpanan hendaklah direkodkan.',
      more_info: {}
    },

    {
      image: require('./penyimpanan_3.png'),
      text: 'Jumlah jualan perlu direkodkan dengan name pembeli/agen bagi memudahkan tindakan pengesanan semula dijalankan.',
      more_info: {}
    }
  ]
};
