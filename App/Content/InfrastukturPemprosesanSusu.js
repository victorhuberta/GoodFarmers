module.exports = {
  title: 'Infrastruktur Pemprosesan Susu',
  description: ``,
  steps: [
    {
      image: require('./infra_1.png'),
      text: 'Premis hendaklah berdaftar dengan DVS.',
      more_info: {}
    },

    {
      image: require('./pengendalian_7.png'),
      text: 'Bilik yang digunakan untuk pempasteuran susu hendaklah menepati criteria tertentu.',
      more_info: {
        image: require('./extras2.gif'),
        text: `
i)  Diasingkan daripada bilik-bilik yang tidak berkenaan.
ii) Bersih dan bebas pencemaran.
iii) Peralatan dan pekakasan pempastueran lengkap.
iv) Peralatan dan pekakas perlu bersih dan dinyahkuman.
iiv) Ruang khas untuk menyimpan peralatan dan pekakasan pempasteuran.`
      }
    }
  ]
};
