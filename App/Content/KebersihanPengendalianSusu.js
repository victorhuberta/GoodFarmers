module.exports = {
  title: 'Kebersihan Pengendalian Pempasteuran Susu',
  description: ``,
  steps: [
    {
      image: require('./kebersihan_1.png'),
      text: 'Pakaian pekerja hendaklah bersih semasa menjalankan kerja pempasteuran susu',
      more_info: {}
    },

    {
      image: require('./kebersihan_2.png'),
      text: 'Tangan pekerja hendaklah bersih sebelum dan selepas mengendali kerja pempasteuran.',
      more_info: {}
    },

    {
      image: require('./kebersihan_3.png'),
      text: 'Pekerja hendaklah sedaya upaya untuk tidak menyebabkan pencemaran kepada susu seperti merokok dan lain-lain.',
      more_info: {}
    },

  ]
};
