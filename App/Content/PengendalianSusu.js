module.exports = {
  title: 'Pengendalian Susu Yang Diperah',
  description: ``,
  steps: [
    {
      image: require('./pengendalian_1.png'),
      text: 'Tapiskan susu yang diperah',
      more_info: {
        image: require('./tapis_susu.png'),
        text: 'Tapis susu dengan kaedah yang betul.'
      }
    },

    {
      image: require('./pengendalian_2.png'),
      text: 'Timbangkan susu yang telah ditapis.',
      more_info: {
        image: require('./timbang_susu.png'),
        text: 'Timbangkan susu yang telah ditapis'
      }
    },

    {
      image: require('./pengendalian_3.png'),
      text: 'Rekodkan susu yang telah di timbang dan di tapis.',
      more_info: {}
    },

    {
      image: require('./pengendalian_4.png'),
      text: 'Susu yang telah diperah hendaklah dibawa ke premis pemprosesan dengan segera.',
      more_info: {}
    },

    {
      image: require('./pengendalian_5.png'),
      text: 'Peralatan untuk mengendalikan susu perlulah mengikuti criteria tertentu: ',
      more_info: {}
    },

    {
      image: require('./pengendalian_6.png'),
      text: 'Setelah selesai pemerahan, celupkan puting susu dengan cecair iosan.',
      more_info: {}
    },

    {
      image: require('./pengendalian_7.png'),
      text: 'Peralatan pemerahan perlu dicuci dengan bersih dan disimpankan ditempat yang jauh dari pencemaran.',
      more_info: {
        image: require('./extra3.gif'),
        text: `
i)  Bersih
ii) Tidak toksik
iii) Tidak menyerap bau
iv) Tahan hakisan
iiv) Mudah didcuci dan dinyahkuman`
      }
    },

    {
      image: require('./pengendalian_8.png'),
      text: 'Peralatan yang tidak boleh digunakan perlu mengikuti criteria tertentu',
      more_info: {
        image: {},
        text: `
i)  Bercalar
ii) Pecah
iii) Bocor
iv) Sumbing`
      }
    },

    {
      image: require('./pengendalian_9.png'),
      text: 'Susu perlu disimpan pada suhu celcius 4 darjah sekiranya process selanjutnya tidak dapat dilakukan pada hari yang sama untuk tempoh tidak melebihi 3 hari',
      more_info: {}
    }
  ]
};
