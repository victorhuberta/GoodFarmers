/*
 * Contain all global variables needed
 * for this application.
 */

module.exports = {
  SPLASH_SCREEN: 0,
  MAIN_MENU: 1,
  MENU_CONTENT: 2,
  MORE_INFO: 3
};
